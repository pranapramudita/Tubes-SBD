package tubes;
import java.io.*;
import java.util.*;
public class TubesSBD {
    
/*  
    Kelas : IF-41-01
    Daftar Kelompok :
    1. Tri Jaka Pamungkas (1301170306)
    2. Prana Pramudita Kusdiananggalih (1301174059)
    3. Muhammad Fadhil Zakaria (1301174096)
    4. Lula M. Aflah (1301170342)
*/    
    static int lengthAfter(String[] column){
        // mencari panjang array sesungguhnya
        int length = 0;
        int o = 0;
        while(o != column.length){
            if(column[o] != null){
                length++;
            }
            o++;
        }
        return length;
    }
    
    static String[] separate (String query, String separator){
        // Memisahkan setiap kata pada kalimat berdasarkan tipe data separator
        String[] word = query.split(separator);
        return word;
    }
    
    static String[][] convertToArray (File file) throws FileNotFoundException{
        //Mengubah text didalam Data.txt menjadi string array
        Scanner sc = new Scanner(file);
        String[][] database = new String[4][]; //4 tabel
        int i = 0;        
        while(sc.hasNextLine()){
            database[i] = separate(sc.nextLine(), ";"); // separate by semicolon
            i++;
        }
        return database;
    }
    
    static void viewAllDatabase(String[][] database){
        //View All Tabel and Column
        System.out.println("//DATABASE//");
        for (int i = 0; i != database.length; i++) {
            for (int j = 0; j != database[i].length; j++) {
                if(j==0){
                    System.out.print("Tabel: ");
                    System.out.println(database[i][j]);
                }else if(j==1){
                    System.out.println("Atribut:");
                    System.out.println("- "+database[i][j]);
                }else{
                    System.out.println("- "+database[i][j]);
                }
            }
            System.out.println();
        }
    }
    
    static boolean checkArrayTable(String[] table, String[][] database){
        // memeriksa apakah array table ada semua dalam database
        boolean[] ada = new boolean[lengthAfter(table)];
        boolean getTable1 = true;
        int k = 0;
        while(k != lengthAfter(table)){
//            System.out.println(k+". table : "+column[k]);
            for (int i = 0; i != database.length; i++) {
//                System.out.print("db : "+database[i][0]+" kondisi : ");
//                System.out.println(table[k].equals(database[i][0]));
                if(table[k].equals(database[i][0])){
                    ada[k] = true;
                }
            }
            k++;
//            System.out.println();
        }
        int j = 0;
//        System.out.println("cek");
        while(j != ada.length){
//            System.out.println(ada[j]);
            if(ada[j] == false){
                getTable1 = false;
            }j++;
        }
        return getTable1;
    }
    
    static boolean checkOneTable(String table, String[][] database){
        // memeriksa apakah table ada dalam database
        boolean ada = false;
        for (int i = 0; i < database.length; i++) {
            if(database[i][0].equals(table)){
                ada = true;
            }
        }
        return ada;
    }
    
    static boolean checkOneColumn(String Column, String[][] database){
        // memeriksa apakah Column ada dalam database
        boolean ada = false;
        for (int i = 0; i < database.length; i++) {
            for (int j = 1; j < database[i].length; j++) {
                if(database[i][j].equals(Column)){
                    ada = true;
                }   
            }
            
        }
        return ada;
    }
    
    static int getArrayTable(String table, String[][] database){
        // return array ke berapa table yang dimasukkan
        int ada = -1;
        for (int i = 0; i < database.length; i++) {
            for (int j = 0; j < database[i].length; j++) {
                if(table.equals(database[i][j])){
                    ada = i;
                }
            }
        }
        return ada;
    }
    
    static String[] getTableQuery(String query, String[][] database){
        //Get array table from query
        String[] statement = separate(query, " ");
        String lastword = statement[statement.length - 1];
        statement[statement.length - 1] = lastword.replace(";", "");
        String[] table = new String[10];
        String[] temp = new String[10];
        String[] alltable = new String[3];
        int a = 0;
        for (int i = 0; i != statement.length; i++){
            if(statement[i].equals("from")){
                a = i;
            }
        }
        int b = 0;
        for (int i = a+1; i < statement.length; i++) {
            if(checkOneTable(statement[i], database)){
                table[b] = statement[i];
                b++;
            }
        }
        
//        int i = 0;
//        while(table[i] != null){
//            System.out.println(i);
//            System.out.println(table[i]);
//            i++;
//        }
        return table;
    }
    
    static boolean checkAllColumn(String[] column, String[][] database){
        // memeriksa apakah array kolom ada semua dalam tabel database
        boolean[] ada = new boolean[lengthAfter(column)];
        boolean getColumn1 = true;
        int k = 0;
        while(k != lengthAfter(column)){
//            System.out.println(k+". column : "+column[k]);
            for (int i = 0; i != database.length; i++) {
                for (int j = 1; j != database[i].length; j++){
//                    System.out.print("db : "+database[i][j]+" kondisi : ");
//                    System.out.println(column[k].equals(database[i][j]));
                    if(column[k].equals(database[i][j])){
                        ada[k] = true;
                    }
                }
            }
            k++;
//            System.out.println();
        }
        int j = 0;
//        System.out.println("cek");
        while(j != ada.length){
//            System.out.println(ada[j]);
            if(ada[j] == false){
                getColumn1 = false;
            }j++;
        }
        return getColumn1;
    }

    static String[] getColumnByTable(String table, String[][] database){
        // Return All Column by table 
        String[] column = new String[10];
        if(checkOneTable(table, database)){
            for (int i = 1; i != database[getArrayTable(table, database)].length-3; i++) {
                column[i-1] = database[getArrayTable(table, database)][i];
            }
        }
//        for (int i = 0; i != lengthAfter(column); i++) {
//            System.out.println(column[i]);
//        }
        return column;
    }    
    
    static boolean getUsingColumn(String column, String[] columnTable){
        boolean ada = false;
        for (int j = 0; j < lengthAfter(columnTable); j++) {
            if(column.equals(columnTable[j])){
                ada = true;
                break;
            }
        }
        return ada;
    }
    
    static String[] getColumnQuery(String query, String[][] database){
        // get array column by query
        String[] statement = separate(query, " ");
        String[] column = new String[10];
        String[] temp = new String[10];
        int a = 0,b = 0;
        for (int i = 0; i != statement.length; i++){
            if(statement[i].equals("select")){
                a = i;
            }if(statement[i].equals("from")){
                b = i;
            }
        }
        if(statement[a+1].equals("*")){
            String[] table = new String[10];
            table = getTableQuery(query, database);
//            System.out.println(table[0]);
            column = getColumnByTable(table[0], database);
        }else{
            for (int i = a+1; i <= b-1; i++) {
                if(statement[i].contains(",")){
                    String lastword = statement[1];
                    char lastcomma = lastword.charAt(lastword.length() - 1);
                    if(lastcomma != ','){
                        temp = separate(statement[i], ",");
                        int k = 0;
                        int m = 0;
                        while(k != temp.length){
                            column[m] = temp[k];
                            m++;
                            k++;
                        }
                    }else{
                        column[i-1] = statement[i].replace(",", "");
                    }
                }else{
                    column[i-1] = statement[i];
                }
            }   
        }
//        int i = 0;
//        while(column[i] != null){
//            System.out.println(i);
//            System.out.println(column[i]);
//            i++;
//        }
        return column;
    }
    
    static String getUsing(String query, String[][] database){
        String using = null;
        String[] statement = separate(query, " ");
        for (int i = 0; i < statement.length; i++){
            if(statement[i].equals("using")){
                using = statement[i+1];
                using = using.replace("(", "");                
                using = using.replace(")", "");
                using = using.replace(";", "");
            }
        }
        return using;
    }
    
    static String[] getWhere(String query, String[][] database){
        String where[] = new String[3];
        String[] statement = separate(query, " ");
        for (int i = 0; i < statement.length; i++){
            if(statement[i].equals("where")){
                where[0] = statement[i+1];                
                where[1] = statement[i+3];
                where[1] = where[1].replace(";","");
            }
        }
        return where;
    }
    
    static Boolean executeQuery (String query, String[][] database, String[][] pb) throws FileNotFoundException{
        query = query.toLowerCase();
        String[] statement = separate(query, " ");
        String lastword = statement[statement.length - 1];
        char semicolon = lastword.charAt(lastword.length() - 1);
        
        String[][] get = getRnV(database);
        int p = Integer.parseInt(pb[0][0]);        
        int b = Integer.parseInt(pb[0][1]);
        
        if(semicolon != ';'){
            System.out.println("SQL Error (Missing ;)");
            return false;
        }else{
            statement[statement.length - 1] = lastword.replace(";", "");
            String[] table = new String[10];            
            String[] column = new String[10];
            boolean selectBool = false;
            boolean fromBool = false;
            boolean whereBool = false;
            boolean joinBool = false;
            boolean usingBool = false;            
            boolean onBool = false;
            int[] cost = new int[3];
            
            for (int i = 0; i != statement.length; i++) {
                if(statement[i].equals("select")){
                    selectBool = true;
                }
                if(statement[i].equals("from")){
                    fromBool = true;
                }
                if(statement[i].equals("where")){
                    whereBool = true;
                }
                if(statement[i].equals("join")){
                    joinBool = true;
                }
                if(statement[i].equals("using")){
                    usingBool = true;
                }
                if(statement[i].equals("on")){
                    onBool = true;
                }
            }

            if(selectBool == false){
                System.out.println("SQL Error (Syntax Error)");
            }else{
                if(fromBool == false){
                    System.out.println("SQL Error (Syntax Error)");
                }else{
                    ////// OUTPUT VIEW //////////
                    table = getTableQuery(query, database);
                    column = getColumnQuery(query, database);
                    if(whereBool == true){
                        String[] where = getWhere(query,database);
//                        System.out.println((checkAllColumn(column, database)));
                        if((checkArrayTable(table, database)) && (checkAllColumn(column, database)) && (checkOneColumn(where[0], database))){                         
                            // SELECT FROM WHERE
                            for (int i = 0; i < column.length; i++) {
                                if(table[i] != null){
                                    i++;
                                    System.out.println("Tabel ("+ i +") : "+table[i-1]);
                                    System.out.print("List Kolom : ");
                                    for (int j = 0; j < lengthAfter(column); j++) {
                                        System.out.print(column[j]+", ");
                                    }System.out.println(); 
                                    i--;
                                }
                            }
                            
                            // QEP
                            for (int i = 1; i < 3; i++) {
                                System.out.println("QEP #"+i);
                                System.out.print("PROJECTION ");
                                for (int j = 0; j < lengthAfter(column); j++) {
                                    System.out.print(column[j]+", ");
                                }System.out.println();
                                System.out.print("SELECTION ");
                                System.out.println(where[0]+" = "+where[1]);
                                System.out.println(table[0]);
                                
                                int r = Integer.parseInt(get[getArrayTable(table[0], database)][0]);
                                int n = Integer.parseInt(get[getArrayTable(table[0], database)][1]);
                                int v = Integer.parseInt(get[getArrayTable(table[0], database)][2]);
                                
                                if(i == 1){
                                    cost[i] = blockData(BFR(b, r),n)/2;
                                }else{
                                    cost[i] = 1+(int)(Math.log(blockData(BFR(b, n),n))/Math.log(fanout(b, v, p)));
                                }
                                System.out.println("BFR : "+BFR(b, r));
                                System.out.println("Cost : "+cost[i]+" blok");
                                System.out.println();
                            }
                            int op = 1;
                            for (int i = 1; i < 3; i++) {
                                if(cost[i]<cost[op]){
                                    op = i;
                                }
                            }
                            System.out.println("QEP optimal : "+op);
                        }else{
                            System.out.println("SQL Error (Syntax Error)");
                        }
                    }else{
                        if(joinBool == false){
    //                        System.out.println(checkArrayTable(table, database));
    //                        System.out.println(checkAllColumn(column, database));
                            if((checkArrayTable(table, database)) && (checkAllColumn(column, database))){                         
                                // SELECT * FROM
                                for (int i = 0; i < column.length; i++) {
                                    if(table[i] != null){
                                        i++;
                                        System.out.println("Tabel ("+ i +") : "+table[i-1]);
                                        System.out.print("List Kolom : ");
                                        for (int j = 0; j < lengthAfter(column); j++) {
                                            System.out.print(column[j]+", ");
                                        }System.out.println(); 
                                        i--;
                                    }
                                }
                            }else{
                                System.out.println("SQL Error (Syntax Error)");
                            }
                        }else{
                            if(usingBool == false){
                                System.out.println("SQL Error (Syntax Error)");
                            }else{
                                // SELECT * FROM tabel1 JOIN tabel2 USING (atribut1);
                                String using = getUsing(query, database);
                                if((checkArrayTable(table, database)) && (checkAllColumn(column, database)) && (checkOneColumn(using, database))){       
                                    for (int i = 0; i < lengthAfter(table); i++) {
                                        if(table[i] != null){
                                            i++;
                                            System.out.println("Tabel ("+ i +") : "+table[i-1]);
                                            System.out.print("List Kolom : ");
                                            for (int j = 0; j < lengthAfter(column); j++) {
                                                if(getUsingColumn(column[j], getColumnByTable(table[i-1], database))){
                                                    System.out.print(column[j]+", "); 
                                                }
                                            }System.out.println();
                                            i--;
                                        }
                                    }System.out.println();
                                    
                                // QEP
                                for (int i = 1; i < 3; i++) {
                                    System.out.println("QEP #"+i);
                                    System.out.print("PROJECTION ");
                                    for (int j = 0; j < lengthAfter(column); j++) {
                                        System.out.print(column[j]+", ");
                                    }System.out.println();
                                    System.out.print("JOIN ");
                                    System.out.println(table[0]+"."+using+" = "+table[1]+"."+using);
                                    if(i == 1){
                                        System.out.print(table[0]+" "+table[1]+ " --- Block Nested Loop ");
                                    }else{
                                        System.out.print(table[1]+" "+table[0]+ " --- Block Nested Loop ");
                                    }System.out.println(i);
                                    int r0 = Integer.parseInt(get[getArrayTable(table[0], database)][0]);                                    
                                    int r1 = Integer.parseInt(get[getArrayTable(table[1], database)][0]);
                                    int n0 = Integer.parseInt(get[getArrayTable(table[0], database)][1]);                                    
                                    int n1 = Integer.parseInt(get[getArrayTable(table[1], database)][1]);
                                    int v0 = Integer.parseInt(get[getArrayTable(table[0], database)][2]);                                    
                                    int v1 = Integer.parseInt(get[getArrayTable(table[1], database)][2]);
                                    
                                    //REVISI +NESTED LOOP
                                    if(i == 1){
                                        cost[i] = (blockData(BFR(b, r0),n0) * blockData(BFR(b, r1),n1)) + blockData(BFR(b, r0),n0);
                                        System.out.println("BFR "+table[0]+" : "+BFR(b, r0));
                                    }else{
                                        cost[i] = (blockData(BFR(b, r1),n1) * blockData(BFR(b, r0),n0)) + blockData(BFR(b, r1),n1);
                                        System.out.println("BFR "+table[1]+" : "+BFR(b, r1));
                                    }
                                    
                                    System.out.println("Cost : "+cost[i]+" blok");
                                    System.out.println();
                                }
                            int op = 1;
                            for (int i = 1; i < 3; i++) {
                                if(cost[i]<cost[op]){
                                    op = i;
                                }
                            }
                            System.out.println("QEP optimal : QEP#"+op);
                                }else{
                                    System.out.println("ada yang ga ada");
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }
    }
    
static Boolean menu5exe (String query, String[][] database, String[][] pb) throws FileNotFoundException{
        query = query.toLowerCase();
        String[] statement = separate(query, " ");
        String lastword = statement[statement.length - 1];
        char semicolon = lastword.charAt(lastword.length() - 1);
        
        String[][] get = getRnV(database);
        int p = Integer.parseInt(pb[0][0]);        
        int b = Integer.parseInt(pb[0][1]);
        
        if(semicolon != ';'){
            System.out.println("SQL Error (Missing ;)");
            return false;
        }else{
            statement[statement.length - 1] = lastword.replace(";", "");
            String[] table = new String[10];            
            String[] column = new String[10];
            boolean selectBool = false;
            boolean fromBool = false;
            boolean whereBool = false;
            boolean joinBool = false;
            boolean usingBool = false;            
            boolean onBool = false;
            int[] cost = new int[3];
            
            for (int i = 0; i != statement.length; i++) {
                if(statement[i].equals("select")){
                    selectBool = true;
                }
                if(statement[i].equals("from")){
                    fromBool = true;
                }
                if(statement[i].equals("where")){
                    whereBool = true;
                }
                if(statement[i].equals("join")){
                    joinBool = true;
                }
                if(statement[i].equals("using")){
                    usingBool = true;
                }
                if(statement[i].equals("on")){
                    onBool = true;
                }
            }

            if(selectBool == false){
                System.out.println("SQL Error (Syntax Error)");
            }else{
                if(fromBool == false){
                    System.out.println("SQL Error (Syntax Error)");
                }else{
                    ////// OUTPUT VIEW //////////
                    table = getTableQuery(query, database);
                    column = getColumnQuery(query, database);
                    if(whereBool == true){
                        String[] where = getWhere(query,database);
//                        System.out.println((checkAllColumn(column, database)));
                        if((checkArrayTable(table, database)) && (checkAllColumn(column, database)) && (checkOneColumn(where[0], database))){                         
                            // SELECT FROM WHERE
                            
                            // QEP
                            for (int i = 1; i < 3; i++) {
                                int r = Integer.parseInt(get[getArrayTable(table[0], database)][0]);
                                int n = Integer.parseInt(get[getArrayTable(table[0], database)][1]);
                                int v = Integer.parseInt(get[getArrayTable(table[0], database)][2]);
                                
                                if(i == 1){
                                    cost[i] = blockData(BFR(b, r),n)/2;
                                }else{
                                    cost[i] = 1+(int)(Math.log(blockData(BFR(b, r),n))/Math.log(fanout(b, v, p)));
                                }
                            }
                            int op = 1;
                            for (int i = 1; i < 3; i++) {
                                if(cost[i]<cost[op]){
                                    op = i;
                                }
                            }
                            
                            // Optimal
                            System.out.print("PROJECTION ");
                            for (int j = 0; j < lengthAfter(column); j++) {
                                System.out.print(column[j]+", ");
                            }System.out.println("-- on the fly");
                            System.out.print("SELECTION ");
                            System.out.println(where[0]+" = "+where[1]);
                            System.out.println(table[0]);
                            System.out.println("Cost : "+cost[op]+" blok");
                            
                        }else{
                            System.out.println("SQL Error (Syntax Error)");
                        }
                    }else{
                        if(joinBool == false){
    //                        System.out.println(checkArrayTable(table, database));
    //                        System.out.println(checkAllColumn(column, database));
                            if((checkArrayTable(table, database)) && (checkAllColumn(column, database))){                         
                                // SELECT * FROM
                                for (int i = 0; i < column.length; i++) {
                                    if(table[i] != null){
                                        i++;
                                        System.out.println("Tabel ("+ i +") : "+table[i-1]);
                                        System.out.print("List Kolom : ");
                                        for (int j = 0; j < lengthAfter(column); j++) {
                                            System.out.print(column[j]+", ");
                                        }System.out.println(); 
                                        i--;
                                    }
                                }
                            }else{
                                System.out.println("SQL Error (Syntax Error)");
                            }
                        }else{
                            if(usingBool == false){
                                System.out.println("SQL Error (Syntax Error)");
                            }else{
                                // SELECT * FROM tabel1 JOIN tabel2 USING (atribut1);
                                String using = getUsing(query, database);
                                if((checkArrayTable(table, database)) && (checkAllColumn(column, database)) && (checkOneColumn(using, database))){
                                    // QEP
                                    for (int i = 1; i < 3; i++) {

                                        int r0 = Integer.parseInt(get[getArrayTable(table[0], database)][0]);                                    
                                        int r1 = Integer.parseInt(get[getArrayTable(table[1], database)][0]);
                                        int n0 = Integer.parseInt(get[getArrayTable(table[0], database)][1]);                                    
                                        int n1 = Integer.parseInt(get[getArrayTable(table[1], database)][1]);
                                        int v0 = Integer.parseInt(get[getArrayTable(table[0], database)][2]);                                    
                                        int v1 = Integer.parseInt(get[getArrayTable(table[1], database)][2]);

                                        if(i == 1){
                                            cost[i] = (blockData(BFR(b, r0),n0) * blockData(BFR(b, r1),n1)) + blockData(BFR(b, r0),n0);
                                        }else{
                                            cost[i] = (blockData(BFR(b, r1),n1) * blockData(BFR(b, r0),n0)) + blockData(BFR(b, r1),n1);
                                        }
                                    }
                                    int op = 1;
                                    for (int i = 1; i < 3; i++) {
                                        if(cost[i]<cost[op]){
                                            op = i;
                                        }
                                    }

                                    // Optimal
                                    System.out.print("PROJECTION ");
                                    for (int j = 0; j < lengthAfter(column); j++) {
                                        System.out.print(column[j]+", ");
                                    }System.out.println("-- on the fly");
                                    System.out.print("JOIN ");
                                    System.out.println(table[0]+"."+using+" = "+table[1]+"."+using);
                                    if(op == 1){
                                        System.out.print(table[0]+" "+table[1]);
                                    }else{
                                        System.out.print(table[1]+" "+table[0]);
                                    }System.out.println();
                                    System.out.println("Cost : "+cost[op]+" blok");
                                }else{
                                    System.out.println("ada yang ga ada");
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }
    }
    
    static String inputQuery(){
        System.out.print("Input Query: ");
        Scanner sc = new Scanner(System.in);
        String query = sc.nextLine();
        query = query.toLowerCase();
        System.out.println();
        return query;
    }
    
    static String[][] getRnV(String[][] database){
        String[][] get = new String[4][11];
        for (int k = 0; k < 4; k++) {
//            System.out.println(database[k][0]);                
//            System.out.println(database[k].length);
            int j = 0;
            for (int i = (database[k].length)-3; i < database[k].length; i++) {
//                System.out.println(database[k][i]);
                    get[k][j++] = database[k][i];
            }  
        }
        return get;
    }
    
    static void menu1(String[][] database, String[][] pb){
        //// BFR dan Fan Out Ratio
        String[][] get = getRnV(database);
        int p = Integer.parseInt(pb[0][0]);        
        int b = Integer.parseInt(pb[0][1]);
        System.out.println("MENU 1: BFR dan Fan Out Ratio");
//        System.out.println("P = "+p);
//        System.out.println("B = "+b);
        System.out.println();
        for (int i = 0; i < get.length; i++) {
//            System.out.println("Tabel : "+database[i][0]);
//            for (int j = 0; j < get[i].length; j++) {
//                if(get[i][j]!=null){
//                    if(j == 0){
//                        System.out.println("R = "+get[i][j]);
//                    }else if(j == 1){
//                        System.out.println("n = "+get[i][j]);
//                    }else if(j == 2){
//                        System.out.println("V = "+get[i][j]);
//                    }
//                }
//            }
            int r = Integer.parseInt(get[i][0]);
            int n = Integer.parseInt(get[i][1]);
            int v = Integer.parseInt(get[i][2]);
            System.out.println("BFR "+database[i][0]+" : "+BFR(b,r));
            System.out.println("Fanout Ratio "+database[i][0]+" : "+fanout(b,v,p));
//            System.out.println();
        }
    }
    
    static int BFR(int B, int R){
        return (int) Math.floor(B/R);
    }
    
    static int fanout(int B, int V, int P){
        return (int) Math.floor(B/(V + P));
    }
    
    static void menu2(String[][] database, String[][] pb){
        /// Jumlah blok ///
        String[][] get = getRnV(database);
        int p = Integer.parseInt(pb[0][0]);        
        int b = Integer.parseInt(pb[0][1]);
        System.out.println("MENU 2: Jumlah blok");
        System.out.println();
        for (int i = 0; i < get.length; i++) {
            int r = Integer.parseInt(get[i][0]);            
            int n = Integer.parseInt(get[i][1]);
            int v = Integer.parseInt(get[i][2]);
            System.out.println("Tabel Data "+database[i][0]+" : "+blockData(BFR(b,r),n)+" blok");
            System.out.println("Indeks "+database[i][0]+" : "+indeksData(fanout(b,v,p), n)+" blok");
        }
    }
    
    static int blockData(int BFR, int n){
        return (n/BFR);
    }
    
    static int indeksData(int fanout, int n){
        return (n/fanout);
    }
    
    static void menu3(String[][] database, String[][] pb){
        /// Pencarian Rekord ///
        String[][] get = getRnV(database);
        int p = Integer.parseInt(pb[0][0]);        
        int b = Integer.parseInt(pb[0][1]);
        System.out.println("MENU 3: Pencarian Rekord");
        System.out.println();
        System.out.println("Input :");
        System.out.print("Cari Rekord ke- : ");
        Scanner sc1 = new Scanner(System.in);
        int inp1 = sc1.nextInt();
        System.out.print("Nama Tabel : ");
        Scanner sc2 = new Scanner(System.in);
        String inp2 = sc2.nextLine();
        System.out.println();
        System.out.println("Output : ");
        if(getArrayTable(inp2, database) != -1){
            int i = getArrayTable(inp2.toLowerCase(), database);
            int r = Integer.parseInt(get[i][0]);            
            int n = Integer.parseInt(get[i][1]);
            int v = Integer.parseInt(get[i][2]);
            System.out.println("Menggunakan indeks, jumlah blok yang diakses : "+wIdxBlock(inp1,fanout(b,v,p))+" blok");
            System.out.println("Tanpa indeks, jumlah blok yang diakses : "+woIdxBlock(inp1,BFR(b,r))+" blok");
        }else{
            System.out.println("TABEL NOT FOUND");
        }
    }
    
    static int wIdxBlock(int input, int fanout){
        return (int) Math.ceil((input/fanout)+1);
    }
    
    static int woIdxBlock(int input, int bfr){
        return (int) Math.ceil(input/bfr);
    }
    
    static void menu4(String[][] database, String[][] pb) throws FileNotFoundException{
        System.out.println("MENU 4: QEP dan Cost");
        String query;
//        query = inputQuery();
//        query = "SELECT id_dokter, nama_dokter FROM dokter WHERE id_dokter = 190;";        
        query = "SELECT id_dokter,nama_dokter,nama_rs,id_rs FROM dokter JOIN rs USING (id_rs);";
        System.out.println();
        System.out.println("Input Query: "+query);
        System.out.println();
        System.out.println("Output: ");
        Boolean Output = executeQuery(query,database,pb);
    }
    
    static void menu5(String[][] database, String[][] pb) throws FileNotFoundException{
        System.out.println("Menu 5: Shared Pool");
        String query1,query2;
        query1 = "SELECT id_dokter, nama_dokter FROM dokter WHERE id_dokter = 190;";        
        query2 = "SELECT id_dokter,nama_dokter,nama_rs,id_rs FROM dokter JOIN rs USING (id_rs);";
        System.out.println();
        System.out.print("Query : ");
        System.out.println(query1);
        Boolean Output = menu5exe(query1,database,pb);  
        System.out.println();
        System.out.print("Query : ");     
        System.out.println(query2);
        Output = menu5exe(query2,database,pb);
    }
    
    public static void main(String[] args) throws FileNotFoundException {
        File file1 = new File("database/Data.txt");
        String[][] database = convertToArray(file1);
        File file2 = new File("database/PB.txt");
        String[][] pb = convertToArray(file2);

        ///LIST QUERY TINGGAL UNCOMMENT///
        String query;
//        query = inputQuery();
//        query = "select * from dokter;";       
//        query = "select * from obat;";     
//        query = "select * from rs;";     
//        query = "select * from pasien;";     
//        query = "select id_dokter, nama_dokter from dokter;";
//        query = "SELECT id_dokter,nama_dokter,nama_rs,id_rs FROM dokter JOIN rs USING (id_rs);";
//        query = "SELECT * FROM dokter JOIN rs USING (id_rs);";
//        Boolean output = executeQuery(query, database);
        
        int pilih = -1;
        while (pilih != 6) {
            System.out.println("Menu Utama:");
            for (int i = 1; i < 6; i++) {
                System.out.println(i+". Menu "+i);
            }System.out.println("6. Keluar");
            System.out.print("Masukan Pilihan Anda: ");
            Scanner sc = new Scanner(System.in);
            pilih = sc.nextInt();
            System.out.println();
            switch(pilih){
                case 1:
                    menu1(database,pb);
                    break;
                case 2:
                    menu2(database,pb);
                    break;
                case 3:
                    menu3(database,pb);
                    break;
                case 4:
                    menu4(database,pb);
                    break;
                case 5:
                    menu5(database,pb);
                    break;
            }System.out.println();
        }

//  COBACOBA  
//        System.out.println(output);
//        getAllTabel(database);
//        for (int i = 0; i < word.length; i++) {
//            System.out.println(word[i]);
//        }
//        System.out.println(dictionary.length);
//        System.out.println(dictionary[0].length);
//        System.out.println(dictionary[1].length);
//        System.out.println(dictionary[2].length);
//        System.out.println(dictionary[3].length);

//        System.out.println(dictionary[0][0]);
//        System.out.println(dictionary[1][0]);
    }
}
