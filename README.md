# Tubes-SBD
Tugas Besar Sistem Basis Data

email Pak Ario : ariohp@telkomuniversity.ac.id

Daftar Kelompok :
1. Tri Jaka Pamungkas (1301170306)
2. Prana Pramudita Kusdiananggalih (1301174059)
3. Muhammad Fadhil Zakaria (1301174096)
4. Lula M. Aflah (1301170342)

# Tubes 1
Buatlah aplikasi SEDERHANA untuk mem-parsing SQL statement dasar.
Aplikasi dibangun dengan menggunakan Java console TANPA GUI.
SQL statement harus memiliki : (1). List kolom yang dipakai, (2). Join minimal 2 table.

Prerequisites   : (1). ERD yang sesuai dengan Data Dictionary yang dipakai.
                  (2). Minimum 2 tabel dengan relasi m-m

Input Aplikasi  : (1). Siapkan Data Dictionary untuk menampung semua nama tabel beserta kolomnya.
                       Data dictionary disimpan pada file text.
                  (2). SQL statement

Output aplikasi : (1). Menampilkan nama tabel dan nama kolom untuk setiap tabel.
                  (2). Menolak pemrosesan jika SQL Statement tidak valid.
                  (3). Penjelasan Error message optional
		  (4). DILARANG MENGGUNAKAN CONTOH DATA YANG SUDAH ADA DISINI!

Dikumpulkan di minggu ke-7 dan PRESENTASI.

Contoh Data Dictionary :
Misal standar CSV dengan format table_name;col1;col2;col3,...#

Mahasiswa;nim;nama;alamat;ipk#
MataKuliah;kode;nama;sks#
Registrasi;nim;kode;semester;tahun_ajar;nilai#

Contoh Penggunaan Aplikasi:

Masukkan SQL Statement : 
SELECT m.nim,nama,kode,nilai FROM mahasiswa m JOIN registrasi r ON (m.nim=r.nim);

Output :
Tabel (1) : Mahasiswa
List Kolom : nim, nama
Tabel (2) : registrasi
List Kolom : nim, kode, nilai

--------------------------------------------------------------

Masukkan SQL Statement :
SELECT nim,alamat FROM mahasiswa

Output : SQL Error (Missing ;)

---------------------------------------------------------------

Masukkan SQL Statement :
SELECT nim,kode,nilai FORM registrasi;

Output : SQL Error (Syntax Error)

# Tubes 2
GAMBARAN UMUM APLIKASI
>> Menu Utama:
1. Tampilkan BFR dan Fanout Ratio Setiap Tabel
2. Tampilkan Total Blok Data + Blok Index Setiap Tabel
3. Tampilkan Jumlah Blok yang Diakses Untuk Pencarian Rekord
4. Tampilkan QEP dan Cost
5. Tampilkan Isi File Shared Pool
>> Masukan Pilihan Anda: .....

• Menu 1 : BFR dan Fan Out Ratio
BFR Mahasiswa : 64
Fan Out Ratio Mahasiswa : 512
BFR Registrasi : 128
Fan Out Ratio Registrasi : 512
BFR MataKuliah : 64
Fan Out Ratio MataKuliah : 256

• Menu 2 : Jumlah Blok
Tabel Data Mahasiswa : 1563 blok
Indeks Mahasiswa : 196 blok
Tabel Data Registrasi : 8 blok
Indeks Registrasi : 2 blok
Tabel Data MataKuliah : 8 blok
Indeks MataKuliah : 2 blok

• Menu 3 : Pencarian Rekord
Input:
>> Cari Rekord ke- : 5
>> Nama Tabel : Mahasiswa
Output:
>> Menggunakan indeks, jumlah blok yang diakses : 6 blok
>> Tanpa indeks, jumlah blok yang diakses : 15 blok

• Menu 4 : QEP dan Cost
(Contoh #1 – Query BASIC)
Input Query:
>> SELECT nim, nama FROM
Mahasiswa WHERE nim = 190;
Output:
>> Tabel (1) : Mahasiswa
List Kolom : nim, nama
>> QEP #1
PROJECTION nim, nama -- on the fly
SELECTION nim = 190 -- A1 key
Mahasiswa
Cost : 50 block
>> QEP #2
PROJECTION nim, nama -- on the fly
SELECTION nim = 190 -- A2
Mahasiswa
Cost : 10 block
>> QEP optimal : QEP#2
(Contoh #2 – Query JOIN)
Input Query:
>> SELECT nim, nama FROM
Mahasiswa JOIN Registrasi
using (nim);
Output:
>> Tabel (1) : Mahasiswa
List Kolom : nim, nama
Tabel (2) : Registrasi
List Kolom : nim
>> QEP #1
PROJECTION nim, nama -- on the fly
JOIN Mahasiswa.nim = Registrasi.nim -- BNLJ
Mahasiswa Registrasi
Cost (worst case): 30 block
>> QEP #2
PROJECTION nim, nama -- on the fly
JOIN Mahasiswa.nim = Registrasi.nim -- BNLJ
Registrasi Mahasiswa
Cost (worst case): 90 block
>> QEP optimal : QEP#1

• Menu 5 : Shared Pool
1. Query :SELECT nim, nama FROM Mahasiswa WHERE nim = 190;
PROJECTION nim, nama -- on the fly
SELECTION nim = 190 -- A2
Mahasiswa
Cost : 10 block
2. Query : SELECT nim, nama FROM Mahasiswa JOIN Registrasi using (nim);
PROJECTION nim, nama -- on the fly
JOIN mahasiswa.nim = registrasi.nim -- Block Nested loop join
Mahasiswa Registrasi
Cost (worst case): 30 block

CONTOH ISI FILE DATA DICTIONARY
P 4; B 8192#
Mahasiswa;nim,nama,alamat,ipk; R 128; n 100000; V 12 #
MataKuliah;kode,nama,sks; R 8; n 150; V 6 #
Registrasi;nim,kode,semester, tahun_ajar, nilai; R 10; n 1000; V 7 #

CONTOH ISI FILE SHARED POOL
Q#1 : SELECT nim, nama FROM Mahasiswa WHERE nim = 190;
PROJECTION nim, nama -- on the fly
SELECTION nim = 190 -- A2
Mahasiswa
Cost : 10 block
Q#2 : SELECT nim, nama FROM Mahasiswa JOIN Registrasi using (nim);
PROJECTION nim, nama -- on the fly
JOIN mahasiswa.nim = registrasi.nim -- BNLJ
Mahasiswa Registrasi
Cost (worst case): 30 block

KETENTUAN TUGAS BESAR
• Tugas merupakan kelanjutan dari tubes sebelumnya
• Bahasa Pemrograman Bebas.
• Format Data Dictionary dan Shared Pool Bebas.
• Tugas dikumpulkan pada Pekan ke- 13
• Presentasi dilakukan pada Pekan ke- 14

PENILAIAN
CLO1 100
Program
Menu program lengkap (terdiri dari 5
menu) 10
Hitung BFR benar 15
Hitung Fan Out ratio benar 15
Hitung total blok data benar 20
Hitung total blok indek benar 20
Hitung blok berdasar record benar 20
CLO2 100
TUBES 1 20
Program
Data dictionary lengkap sesuai spec 25
Data dictionary tersimpan di file terpisah
Data dictionary merupakan INPUT program 15
Shared pool tersimpan di file terpisah dan
merupakan OUTPUT dari program 15
Shared pool berisi best plan beserta cost 25

CLO3 100
Program 40
Annotated QEP basic query benar 10
Basic query dengan key minimal 2 QEP ( A1 key, A2) 5
Basic query dengan non key (A1 non key) 5
Perhitungan cost BASIC QUERY benar 30
Cost basic query dengan key minimal 2 QEP (A1 key, A2) 5
Cost basic query dengan non key (A1 non key) 5
Annotated QEP join benar minimal 2 QEP (variasi inner/outer relation,
kasus worst-case Block Nested Loop Join) 5
Perhitungan cost JOIN benar 5
Output Pemilihan Best QEP tepat 5
Best QEP yang ditampilkan sama dengan yang tersimpan di shared pool 5
Individu 60
Contoh : mahasiswa diminta menjelaskan logika program yang dibuat untuk
menentukan QEP dan menghitung cost
